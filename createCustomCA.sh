#!/bin/bash
DIR=$(dirname $0)
. $DIR/.COLORS

echoGreen Arbeitsverzeichnis ist $(pwd). Anlegen des Ordners $(pwd)/certs
mkdir -p certs
cd certs

echoGreen Erstellen einer eigenen Zertifizierungsstelle zur Integration in den Browser oder das Smartphone.

echoGreen Erstellen des privaten Schlüssels
echoYellow Vergeben Sie im Folgenden ein mindestens vierstelliges Passwort. Je länger, desto besser.
openssl genrsa -des3 -out rootCA.key 4096

echoGreen Erstellen eines Root-Zertifikats
echoYellow Vergeben Sie einen wiedererkennbaren Common Name
echoYellow Signieren Sie das Zertifikat, indem Sie das eben vergebene Passwort eingeben.
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem

echoGreen Erzeugen eines .crt-Files
openssl x509 -outform der -in rootCA.pem -out rootCA.crt
