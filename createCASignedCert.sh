#!/bin/bash

DIR=$(dirname $0)
. $DIR/.COLORS


if [ ! -d "${DIR}/certs" ] || [ ! -f "${DIR}/certs/rootCA.key" ] || [ ! -f "${DIR}/certs/rootCA.pem" ]; then
    echoRed Keine CA gefunden. Erstelle neue CA.
    ./createCustomCA.sh
else
    echoGreen Root-CA gefunden in "${DIR}/certs"
fi

echoGreen Anlegen der Verzeichnisstruktur
read -p "Vergeben Sie einen Servernamen: " SERVER_NAME
mkdir -p "${DIR}/certs/${SERVER_NAME}"
cd "${DIR}/certs/${SERVER_NAME}"

echoGreen Generiere privaten Schlüssel für Server
echoYellow Vergeben Sie ein server-individuelles Passwort
openssl genrsa -out ${SERVER_NAME}.key 2048

echoGreen Erstelle einen Certificate Signing Request
openssl req -new -sha256 -key ${SERVER_NAME}.key -subj "/C=DE/ST=/O=/CN=${SERVER_NAME}" -out ${SERVER_NAME}.csr

echoGreen Erstelle SAO Informationen
ext="${SERVER_NAME}.ext"
cat > $ext <<EOL
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = ${SERVER_NAME}
EOL

echoGreen Signieren des angeforderten Zertifikats mit dem privaten Key der CA
echoYellow Geben Sie das Passwort für den privaten Schlüssel des Root-Zertifikats an
openssl x509 -req -in ${SERVER_NAME}.csr -CA ../rootCA.pem -CAkey ../rootCA.key -CAcreateserial -out $SERVER_NAME.crt -days 500 -sha256 -extfile $ext

echoGreen Die erstellten Zertifikate wurden im Ordner "${DIR}/certs/${SERVER_NAME}" abgelegt.
