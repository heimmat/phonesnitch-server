ALL: nginx

.PHONY: nginx
nginx: nginx/assets
	pwd

nginx/assets:
	mkdir -p nginx/assets
