#!/usr/bin/env bash

cat << EOF > $GF_PATHS_PROVISIONING/datasources/influx.yaml
apiVersion: 1
datasources:
  # <string, required> name of the datasource. Required
- name: InfluxDB
  # <string, required> datasource type. Required
  type: influxdb
  # <string, required> access mode. proxy or direct (Server or Browser in the UI). Required
  access: proxy
  # <string> url
  url: http://influx:8086
  # <string> database password, if used
  password: $INFLUXDB_READ_USER_PASSWORD
  # <string> database user, if used
  user: $INFLUXDB_READ_USER
  # <string> database name, if used
  database: $INFLUXDB_DB
  # <bool> enable/disable basic auth
  basicAuth: false
  # <bool> enable/disable with credentials headers
  withCredentials: false
  # <bool> mark as default datasource. Max one per org
  isDefault: true
  version: 1
  # <bool> allow users to edit datasources from the UI.
  editable: false
  jsonData:

EOF
cat $GF_PATHS_PROVISIONING/datasources/influx.yaml
