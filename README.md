# PhoneSnitch-Server
This Docker-based server accompanies the [PhoneSnitch-App](https://gitlab.com/heimmat/phonesnitch/).
It provides all the necessary services to get you started capturing and analyzing cell measurements.

## Prerequisites
* Make
* Docker
* Docker-Compose

## Installation
1. Clone this repo on a Linux-based OS
2. Run `make`
3. Create or retrieve a SSL certificate and key
3. Create a `.env`-File containing environment variables to configure the containers during build and runtime. See [Configuration Parameters](#configuration-parameters)
4. Start the server with `docker-compose up -d`

## Troubleshooting
### Containers fail to start
This might be a problem with SELinux. Try running
```bash
./fixSELinux.sh
```

## Configuration Parameters

| Variable name | Used in Container | Description | Range of values | Default | Mandatory |
| ----------------- | -------------------- | ------------ | ------------ | ------- | ----------- |
| ROOT_URL | grafana | Root-Url of this server | A valid URL | | yes |
| INFLUXDB_DB | influx, grafana | Database to create in influx | any string | `PhoneSnitch` | |
| INFLUXDB_ADMIN_USER | influx | Username of influx admin user | any string | `root` ||
| INFLUXDB_ADMIN_PASSWORD | influx | Password of influx admin user | any string |  | yes |
| INFLUXDB_USER | ktor, influx | Username of influx user with write permissions on INFLUXDB_DB | any string | `phoneSnitch` ||
| INFLUXDB_USER_PASSWORD | influx | Password of influx user with write permissions on INFLUXDB_DB | any string |  | yes |
| INFLUXDB_READ_USER | influx, grafana | Username of influx user with read permissions on INFLUXDB_DB | any string | `grafana` ||
| INFLUXDB_READ_USER_PASSWORD | influx, grafana | Password of influx user with read permissions on INFLUXDB_DB  | any string |  |yes|
| ROOT_KEY | nginx | Path to HTTPS-certificate key | a valid file path on host | | yes |
| ROOT_CRT | nginx | Path to HTTPS-certificate cert | a valid file path on host| | yes |
| VOLUMES_PATH | mysql, influx | Path to directory to save persistent container data | a valid path on host |||

## About
The server consists of three separate containers that work together

### Nginx
Nginx is the gateway to the outer world. All external communication passes here. It acts as a Reverse Proxy for the other two containers and has a mounted directory to host files
Copy any file into the `nginx/assets/` directory to make them available at `$ROOT_URL/assets`.

### InfluxDB
This is the central storage for all data created by the [PhoneSnitch-App](https://gitlab.com/heimmat/phonesnitch/). Accessible at `$ROOT_URL/influx`.

### Grafana
Grafana acts as a frontend for Influx and is also proxied by Nginx. Reach it at `$ROOT_URL/grafana`.