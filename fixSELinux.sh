#!/bin/bash
. .env
docker-compose down
sudo chcon -Rt svirt_sandbox_file_t $VOLUMES_PATH./docker
sudo chcon -Rt svirt_sandbox_file_t $ROOT_CRT
sudo chcon -Rt svirt_sandbox_file_t $ROOT_KEY
sudo chcon -Rt svirt_sandbox_file_t ./nginx/assets/
